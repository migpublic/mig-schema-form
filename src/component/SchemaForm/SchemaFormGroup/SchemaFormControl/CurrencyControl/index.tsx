import React from "react";
import { currency } from "../../../../inc/numbers";

interface ICurrencyControlProps {
  onChange: (newValue: number | undefined) => void;
  value?: number;
}

const CurrencyControl = React.memo(
  ({ onChange, value, ...formControlProps }: ICurrencyControlProps) => {
    const [inputValue, setInputValue] = React.useState<string>("");

    React.useEffect(() => {
      setInputValue(value === undefined ? "" : currency(value));
    }, [value]);

    return (
      <input
        type="string"
        value={inputValue}
        onChange={(
          e: React.ChangeEvent<
            HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
          >,
        ) => {
          setInputValue(e.target.value);
          // isFinite(value) ? value : ""
          parseFloat(e.target.value);
        }}
        onBlur={() => {
          if (inputValue.trim() === "") {
            onChange(undefined);
          }
          // normalize "cents"
          const parsedValue = parseFloat(
            inputValue
              // normalize "cents"
              .replace(/\.(\d\d?)$/, ",$1")
              // remove formatting dots
              .replaceAll(".", "")
              // remove , with . for cents
              .replace(",", ".")
              .replaceAll(/[^\d.]/g, ""),
          );
          // pretty format, even when value doesn't change
          setInputValue(value === undefined ? "" : currency(value));
          if (value !== parsedValue) {
            onChange(isFinite(parsedValue) ? parsedValue : undefined);
          } else {
            setInputValue(value === undefined ? "" : currency(value));
          }
        }}
        {...formControlProps}
      />
    );
  },
);

export default CurrencyControl;
