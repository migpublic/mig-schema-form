import React from "react";

interface IIntermediateCheckboxProps {
  disabled?: boolean;
  id?: string;
  required?: boolean;
  value?: boolean;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const IntermediateCheckbox = ({
  disabled,
  id,
  required,
  value,
  onChange,
}: IIntermediateCheckboxProps) => {
  const checkboxRef = React.useRef<HTMLInputElement | null>(null);

  React.useEffect(() => {
    const { current } = checkboxRef;
    if (!current) {
      return;
    }
    if (value === true) {
      current.checked = true;
      current.indeterminate = false;
      return;
    }

    if (required || value === false) {
      current.checked = false;
      current.indeterminate = false;
      return;
    }
    current.checked = false;
    current.indeterminate = true;
  }, [required, value]);

  return (
    <input
      ref={checkboxRef}
      id={id}
      type="checkbox"
      onChange={onChange}
      disabled={disabled}
    />
  );
};

export default React.memo(IntermediateCheckbox);
