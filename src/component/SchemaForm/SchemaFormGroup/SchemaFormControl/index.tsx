import React from "react";
import DatePicker from "react-datepicker";
import Select from "react-select";
import { oas30, oas31 } from "openapi3-ts";
import { formatISO } from "date-fns";
import { isFinite } from "lodash";
import SchemaFormBody from "../../index";
import { localeFormat } from "../../../inc/date";
import reactSelectStyles from "../../react-select/styles";
import MenuList from "../../react-select/MenuList";
import { ISchemaFormControlProps } from "../../../type/field";
import IntermediateCheckbox from "./IntermediateCheckbox";
import CurrencyControl from "./CurrencyControl";

const SchemaFormControl = ({
  form,
  disabled,
  errors,
  instancePath,
  required,
  onChange,
  config,
  name,
  schema,
  translate,
  value,
}: ISchemaFormControlProps) => {
  const formControlProps = {
    autoComplete: "off",
    className: "mig-schema-form-control",
    disabled,
    form,
    id: instancePath,
    name: instancePath,
    required,
    ...config?.controlProps,
  };

  const Control = config?.Control;
  if (Control) {
    return (
      <Control
        errors={errors}
        instancePath={instancePath}
        schema={schema}
        translate={translate}
        onChange={onChange}
        value={value}
        {...formControlProps}
      />
    );
  }

  switch (schema.type) {
    case "boolean":
      return (
        <IntermediateCheckbox
          {...formControlProps}
          value={value}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            onChange(e.target.checked);
          }}
        />
      );

    case "array":
      const itemsSchema = schema.items as
        | oas30.SchemaObject
        | oas31.SchemaObject;
      switch (itemsSchema.type) {
        case "string":
          if (itemsSchema.enum) {
            return (
              <Select
                inputId={instancePath}
                menuPlacement="auto"
                components={{ MenuList }}
                styles={reactSelectStyles}
                options={itemsSchema.enum.map((option) => ({
                  value: option,
                  label: translate(option),
                }))}
                isDisabled={disabled}
                isMulti
                value={(value || []).map((option: string) => ({
                  value: option,
                  label: translate(option),
                }))}
                onChange={(newValue) => {
                  onChange(newValue.map((option: any) => option.value));
                }}
                {...formControlProps}
              />
            );
          }
      }
      console.log(name, itemsSchema);
      throw new Error("Unsupported itemsSchema");

    case "number":
    case "integer":
      if (schema?.format === "currency") {
        return (
          <CurrencyControl
            {...formControlProps}
            onChange={onChange}
            value={value}
          />
        );
      }
      return (
        <input
          type="number"
          value={isFinite(value) ? value : ""}
          onChange={(
            e: React.ChangeEvent<
              HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
            >,
          ) => {
            if (config?.extractValue) {
              onChange(config.extractValue(e));
              return;
            }
            const parsedValue =
              schema.type === "integer"
                ? parseInt(e.target.value)
                : parseFloat(e.target.value);
            onChange(isNaN(parsedValue) ? undefined : parsedValue);
          }}
          min={schema.minimum}
          max={schema.maximum}
          {...formControlProps}
        />
      );

    case "string":
      const onStringInputChange = (
        e: React.ChangeEvent<
          HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
        >,
      ) => {
        // if the string is empty and property is not required, set it to undefined
        let newValue: string | undefined;
        if (config?.extractValue) {
          newValue = config.extractValue(e);
        } else {
          if (e.target.value) {
            newValue = e.target.value;
          } else {
            newValue = required ? "" : undefined;
          }
        }

        onChange(newValue);
      };
      const type = schema.format || "text";

      if (schema.format && ["date", "date-time"].includes(schema.format)) {
        const hasTime = schema.format === "date-time";
        return (
          <DatePicker
            showTimeInput={hasTime}
            showTimeSelect={hasTime}
            timeFormat="HH:mm"
            dateFormat={hasTime ? "dd-MM-yyyy HH:mm" : "dd-MM-yyyy"}
            selected={value ? new Date(value) : null}
            onChange={(date: Date) =>
              onChange(
                date
                  ? hasTime
                    ? formatISO(date)
                    : localeFormat(date, "yyyy-MM-dd")
                  : undefined,
              )
            }
            showMonthDropdown
            showYearDropdown
            {...formControlProps}
          />
        );
      }

      if (schema.enum) {
        return (
          <div style={{ width: 250 }}>
            <Select
              menuPlacement="auto"
              options={schema.enum.map((option) => ({
                value: option,
                label: translate(option),
              }))}
              inputId={instancePath}
              isDisabled={disabled || schema.enum.length < 2}
              value={
                value
                  ? {
                      value: value,
                      label: translate(value),
                    }
                  : null
              }
              onChange={(option) => onChange(option?.value)}
              isClearable={!required}
              {...formControlProps}
            />
          </div>
        );
      }

      return schema.maxLength && schema.maxLength > 512 && !schema.format ? (
        <textarea
          onChange={onStringInputChange}
          value={value || ""}
          {...{
            ...formControlProps,
            style: undefined,
          }}
        />
      ) : (
        <input
          type={type === "uri" ? "url" : type}
          value={value || ""}
          onChange={onStringInputChange}
          {...formControlProps}
        />
      );

    case "object":
      // we can only render a nested form if there is a propValue
      return value ? (
        <SchemaFormBody<any>
          errors={errors}
          instancePath={instancePath}
          schema={schema}
          value={value}
          onChange={onChange}
          translate={translate}
          {...formControlProps}
        />
      ) : null;

    default:
      console.log(schema);
      throw new Error("Unsupported schema");
  }
};

export default React.memo(SchemaFormControl);
