import React, { ReactNode } from "react";
import SchemaFormControl from "./SchemaFormControl";
import { ISchemaFormGroupProps } from "../../type/field";

const SchemaFormGroup = React.memo(
  ({
    config,
    disabled,
    errors,
    form,
    instancePath,
    name,
    required,
    schema,
    onChange,
    translate,
    value,
  }: ISchemaFormGroupProps) => {
    const propValue = value[name];

    const onPropChange = React.useCallback(
      (newValue: any) => {
        // only trigged onChange when there is actually a new value
        if (newValue !== value[name]) {
          onChange({
            ...value,
            [name]: newValue,
          });
        }
      },
      [name, onChange, value],
    );

    const schemaFormControl = React.useMemo<ReactNode>(() => {
      return (
        <SchemaFormControl
          form={form}
          disabled={disabled || config?.disabled}
          errors={errors}
          instancePath={instancePath}
          required={required}
          onChange={onPropChange}
          config={config}
          name={name}
          schema={schema}
          translate={translate}
          value={propValue}
        />
      );
    }, [
      config,
      form,
      disabled,
      errors,
      instancePath,
      required,
      onPropChange,
      name,
      schema,
      translate,
      propValue,
    ]);

    const isLabelUndefined = config ? config.label === undefined : true;
    const isLabelString = config ? typeof config.label === "string" : false;
    const labelText = (
      isLabelString ? config!.label : isLabelUndefined ? name : null
    ) as string | null;

    const className = `mig-schema-form-group mig-schema-form-group--type-${
      schema.type
    } mig-schema-form-group--name-${name}${
      errors.length ? " mig-schema-form-group--error" : ""
    }`;

    switch (schema.type) {
      case "boolean":
        return (
          <div className={className} key={`${name}`}>
            <label className="label" htmlFor={`${instancePath}`}>
              {schemaFormControl}
              {labelText === null ? config!.label : translate(labelText)}
              {/*
              // https://www.notion.so/02f42b8cf8444eb2812940a06586174b?v=5e83c3815f714cebafbdade5777e84d8&p=05f82f5fcaad43a482f4dfb4ec5bfe2e&pm=s
              // Als accountmanager wil ik dat de * bij "Breng de sales-afdeling op de hoogte bij een wijziging" weg gaat, zodat ik niet verward ben dat het verplicht is of niet
              {isRequired ? " *" : ""}*/}
            </label>
          </div>
        );

      case "object":
        return (
          <fieldset key={name} className={className}>
            <legend>{translate(name)}</legend>
            {schemaFormControl}
          </fieldset>
        );

      default:
        const allowedChars =
          schema.maxLength && propValue !== undefined
            ? schema.maxLength - (propValue as string).length
            : 0;
        return (
          <div className={className} key={`${name}`}>
            <div className="mig-schema-form-group__labeled-control">
              {labelText === null ? (
                config?.label || null
              ) : (
                <label className="label" htmlFor={`${instancePath}`}>
                  {translate(labelText)}
                  {required ? " *" : ""}
                </label>
              )}
              <div className="flex-1">
                {schemaFormControl}
                {allowedChars > 0 ? (
                  <div className="mig-schema-form-group__helper-text">
                    {allowedChars === 1
                      ? translate("1 more character allowed")
                      : translate(`{0} more characters allowed`, allowedChars)}
                  </div>
                ) : null}
                {allowedChars < 0 ? (
                  <div
                    className="mig-schema-form-group__helper-text"
                    style={{ color: "red" }}
                  >
                    {allowedChars === -1
                      ? translate("1 character too much")
                      : translate(`{0} characters too much`, -allowedChars)}
                  </div>
                ) : null}
              </div>
            </div>
            {errors.length ? (
              <div className="mig-schema-form-group__error">
                {translate(errors[0].message || "pleaseCheckThisValue")}
              </div>
            ) : null}
          </div>
        );
    }
  },
);

export default React.memo(SchemaFormGroup) as typeof SchemaFormGroup;
