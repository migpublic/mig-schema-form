import React from "react";
import { ErrorObject } from "ajv";
import SchemaFormGroup from "./SchemaFormGroup";
import { trim } from "lodash";
import { oas30, oas31 } from "openapi3-ts";
import { IPropConfig } from "../type/field";
import { uncamel } from "../inc/string";

type ISchemaFormProps<T = Record<string, unknown>> = {
  children?: React.ReactNode;
  className?: string;
  config?: { [propName: string]: IPropConfig };
  disabled?: boolean;
  enableErrorSummary?: boolean;
  errors?: ErrorObject[];
  form?: string;
  instancePath?: string;
  onChange: (value: T) => void;
  schema: oas30.SchemaObject | oas31.SchemaObject;
  translate?: (key: string, ...args: Array<string | number>) => string;
  value: T;
};

function defaultTranslate(key: string, ...args: any[]) {
  let string = uncamel(key);
  args.forEach((arg, index) => {
    string = string.replace(`{${index}}`, arg);
  });
  return string;
}

function SchemaForm<T>({
  children,
  className,
  config,
  disabled,
  enableErrorSummary = true,
  errors,
  form,
  instancePath = "",
  onChange,
  schema,
  translate,
  value,
}: ISchemaFormProps<T>) {
  const t = translate || defaultTranslate;

  const formatError = React.useCallback(
    (error: ErrorObject) => {
      // eg. :"/netfeedrQueries/0"
      const pathNibbles = trim(error.instancePath, "/")
        .split("/")
        .map((nibble) => {
          const isArrayIndex = nibble.match(/^\d+$/);
          // Show array item 0 as element nr. 1 for a normal user
          return isArrayIndex ? `${parseInt(nibble) + 1}` : t(nibble);
        });
      if (!error.instancePath && !error.message) {
        return t("pleaseCheckTheForm");
      }

      return `${error.instancePath.length > 1 ? `${pathNibbles.join(" > ")}: ` : ""}${t(
        error.message || "pleaseCheckThisValue",
      )}`;
    },
    [t],
  );

  const { properties = {}, required = [] } = schema;

  return (
    <div className={`mig-schema-form-body${className ? ` ${className}` : ""}`}>
      {enableErrorSummary && errors?.length ? (
        <div className="alert alert--danger">
          <ul>
            {errors.map((error, errorIndex) => (
              <li key={errorIndex}>{formatError(error)}</li>
            ))}
          </ul>
        </div>
      ) : null}
      {Object.keys(properties)
        .sort((a, b) => {
          const configA = config ? config[a] : undefined;
          const configB = config ? config[b] : undefined;
          const orderA = configA?.order || 0;
          const orderB = configB?.order || 0;
          return orderA - orderB;
        })
        .map((propName) => {
          const propSchema = properties[propName] as
            | oas30.SchemaObject
            | oas31.SchemaObject;
          const propInstancePath = `${instancePath}/${propName}`;
          const propConfig = config ? config[propName] : undefined;
          const FormGroup = propConfig?.Group || SchemaFormGroup;
          const propErrors = errors
            ? errors.filter((error) =>
                error.instancePath?.startsWith(propInstancePath),
              )
            : [];
          if (
            (propSchema.readOnly || propConfig?.hidden) &&
            propConfig?.hidden !== false
          ) {
            return null;
          }
          const isRequired = required.includes(propName);
          return (
            <FormGroup
              config={propConfig}
              disabled={
                propConfig?.disabled === undefined
                  ? disabled
                  : propConfig.disabled
              }
              errors={propErrors}
              form={form}
              instancePath={propInstancePath}
              key={propName}
              name={propName}
              required={isRequired}
              schema={propSchema}
              onChange={onChange}
              translate={t}
              value={value}
              {...propConfig?.groupProps}
            />
          );
        })}
      {children}
    </div>
  );
}

export default React.memo(SchemaForm) as typeof SchemaForm;
