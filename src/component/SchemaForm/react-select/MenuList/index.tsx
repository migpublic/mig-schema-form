import React from "react";
import { FixedSizeList } from "react-window";
import { MenuListProps } from "react-select/dist/declarations/src/components/Menu";

export const REACT_SELECT_MENU_LIST_WIDTH = 400;
export const REACT_SELECT_MENU_ITEM_HEIGHT = 35;

function MenuList<T>(props: MenuListProps<T>): JSX.Element | null {
  const { options, children, maxHeight, getValue } = props;
  const [value] = getValue();
  const initialOffset = options.indexOf(value) * REACT_SELECT_MENU_ITEM_HEIGHT;

  if (!Array.isArray(children)) {
    return null;
  }
  const itemCount = children?.length || 0;

  return (
    <FixedSizeList
      height={Math.min(itemCount * maxHeight, maxHeight)}
      width={REACT_SELECT_MENU_LIST_WIDTH}
      itemCount={itemCount}
      itemSize={REACT_SELECT_MENU_ITEM_HEIGHT}
      initialScrollOffset={initialOffset}
    >
      {({ index, style }) => <div style={style}>{children[index]}</div>}
    </FixedSizeList>
  );
}

export default React.memo(MenuList) as typeof MenuList;
