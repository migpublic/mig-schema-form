export const currency = (value: number, fractionDigits = 2): string =>
    value.toLocaleString("nl-NL", {
        minimumFractionDigits: fractionDigits,
        maximumFractionDigits: fractionDigits,
        style: "currency",
        currency: "EUR",
    });