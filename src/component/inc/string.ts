import { upperFirst } from "lodash";

function isUpperCase(letter: string): boolean {
  return !!letter.match(/[A-Z]/);
}

export function uncamel(camel: string) {
  return upperFirst(
    camel.replaceAll(/[A-Z]/g, (letter, position) => {
      const prevLetter = camel[position - 1];
      const prevIsUppercase = prevLetter ? isUpperCase(prevLetter) : false;
      const nextLetter = camel[position + 1];
      const nextIsUppercase = nextLetter ? isUpperCase(nextLetter) : false;
      const currentIsUppercase = isUpperCase(letter);

      if (
        position === 0 ||
        (prevIsUppercase && (!nextLetter || nextIsUppercase))
      ) {
        return letter;
      }

      if (
        (prevIsUppercase && currentIsUppercase) ||
        (nextIsUppercase && currentIsUppercase)
      ) {
        return letter;
      }

      return ` ${nextIsUppercase ? letter : letter.toLowerCase()}`;
    })
  );
}
