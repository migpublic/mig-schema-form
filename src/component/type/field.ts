import React from "react";
import { oas30, oas31 } from "openapi3-ts";
import { ErrorObject } from "ajv";

export interface ISchemaFormGroupProps<T = any> {
  config?: IPropConfig;
  disabled?: boolean;
  errors: ErrorObject[];
  form?: string;
  instancePath: string;
  name: string;
  onChange: (newValue: T) => void;
  required?: boolean;
  schema: oas30.SchemaObject | oas31.SchemaObject;
  translate: (key: string, ...args: Array<string | number>) => string;
  value: T;
}

export interface ISchemaFormControlProps<T = any> {
  config?: IPropConfig;
  disabled?: boolean;
  errors: ErrorObject[];
  form?: string;
  instancePath: string;
  name: string;
  onChange: (newValue: T) => void;
  required?: boolean;
  schema: oas30.SchemaObject | oas31.SchemaObject;
  translate: (key: string, ...args: Array<string | number>) => string;
  value: T;
}

export interface IPropConfig {
  Control?: React.ComponentType<
    ISchemaFormControlProps & { [controlProp: string]: any }
  >;
  Group?: React.ComponentType<
    ISchemaFormGroupProps & { [groupProp: string]: any }
  >;
  controlProps?: { [controlProp: string]: any };
  disabled?: boolean;
  extractValue?: (e: React.ChangeEvent<any>) => any;
  groupProps?: { [groupProp: string]: any };
  hidden?: boolean;
  label?: React.ReactNode;
  order?: number;
}
