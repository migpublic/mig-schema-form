import React from "react";
import ReactDOM from "react-dom/client";
import { ISchemaFormControlProps, SchemaForm } from "./component";
import "react-datepicker/dist/react-datepicker.css";
import "./component/index.scss";
import { oas31 } from "openapi3-ts";
import Ajv from "ajv";
import type { ErrorObject } from "ajv/lib/types";
import addFormats from "ajv-formats";
import { uncamel } from "./component/inc/string";

const root = ReactDOM.createRoot(
  // @ts-ignore
  document.getElementById("root") as HTMLElement,
);

const nestedObjectSchema: oas31.SchemaObject = {
  type: "object",
  properties: {
    propA: {
      type: "string",
    },
    propB: {
      type: "string",
    },
  },
  required: ["propA"],
};

const testObjectSchema: oas31.SchemaObject = {
  type: "object",
  properties: {
    firstName: {
      type: "string",
      minLength: 3,
    },
    isMember: {
      type: "boolean",
    },
    birthDate: {
      type: "string",
      format: "date",
    },
    activatedDate: {
      type: "string",
      format: "date-time",
    },
    myRequiredInteger: {
      type: "integer",
      minimum: 0,
      maximum: 100,
    },
    role: {
      type: "string",
      enum: ["admin", "user"],
    },
    nestedObjects: {
      type: "array",
      items: nestedObjectSchema,
    },
    hourlyRate: {
      type: "number",
      format: "currency",
    },
  },
  required: ["isMember", "activatedDate", "myRequiredInteger"],
};

interface ITestObject {
  firstName: string;
  nestedObjects?: {}[];
}

const ajv = new Ajv();
addFormats(ajv);
ajv.addFormat("currency", true);
const validate = ajv.compile(testObjectSchema);

const db: { [key: string]: string } = {};

export function t(key: string, ...args: any[]) {
  let string = db[key] || uncamel(key);
  args.forEach((arg, index) => {
    string = string.replace(`{${index}}`, arg);
  });
  return string;
}

const MyCustomControl = React.memo(
  ({
    instancePath,
    translate,
    onChange,
    ...inputProps
  }: ISchemaFormControlProps) => {
    return (
      <input
        onChange={(e) => onChange(e.target.value)}
        {...inputProps}
        name={instancePath}
        id={instancePath}
      />
    );
  },
);

const MyCustomArrayControl = React.memo(
  ({ instancePath, onChange, value = [] }: ISchemaFormControlProps<{}[]>) => {
    return (
      <fieldset>
        {value.map((object, objectIndex) => (
          <SchemaForm
            instancePath={`${instancePath}/${objectIndex}`}
            key={objectIndex}
            schema={nestedObjectSchema}
            value={object}
            onChange={(newNestedObject) => {
              onChange(
                value.map((oldNestedObject, oldNestedObjectIndex) =>
                  oldNestedObjectIndex === objectIndex
                    ? newNestedObject
                    : oldNestedObject,
                ),
              );
            }}
          />
        ))}
      </fieldset>
    );
  },
);

const Test = () => {
  const [testObject, setTestObject] = React.useState<ITestObject>({
    firstName: "",
    nestedObjects: [{}, {}],
  });
  const [errors, setErrors] = React.useState<ErrorObject[]>();

  const onSubmit = React.useCallback(
    (e: React.FormEvent) => {
      e.preventDefault();
      e.stopPropagation();
      validate(testObject);
      console.log(testObject, validate.errors);
      setErrors(validate.errors || []);
    },
    [testObject],
  );

  return (
    <form style={{ margin: 10 }} onSubmit={onSubmit}>
      <SchemaForm<ITestObject>
        schema={testObjectSchema}
        config={{
          firstName: {
            Control: MyCustomControl,
            controlProps: {
              style: {
                border: "1px dashed blue",
              },
            },
          },
          birthDate: {
            controlProps: {
              maxDate: new Date(),
            },
          },
          nestedObjects: {
            label: null,
            Control: MyCustomArrayControl,
          },
        }}
        value={testObject}
        onChange={setTestObject}
        errors={errors}
        translate={t}
      />
      <input type="submit" value="Validate" />
    </form>
  );
};

root.render(<Test />);
